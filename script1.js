// Navbar
$(document).ready(function() {
    $(".fa-bars").click(function() {
      $(".fa-bars").toggle();
      $(".fa-times").toggle();
      $("header nav ul").toggle(1000);
    });
    $(".fa-times").click(function() {
      $(".fa-bars").toggle();
      $(".fa-times").toggle();
      $("header nav ul").toggle(500);
    });
});